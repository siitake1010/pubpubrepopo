model_name = 'testmodel';
stop_time = 3.0;
num_of_cand = 1;
manual_limit = 'limitrange.txt';
dead = {0, 0, 0};
simulation_mode = 'normal';

simout = [];
cv_all = [];

warning('off', 'all');

flag_to_print_coverage = 1;
flag_to_print_structure = 1;

max_number_of_candidates = num_of_cand;

max_processing_time = 3600;

load_system(model_name);

% Initialize parameters of the target model
% set_param(model_name, 'FastRestart','off');
set_param(model_name, 'SimulationMode','normal');
set_param(model_name, 'SFSimEnableDebug','off');
set_param(model_name, 'SFSimEcho','off');
%set_param(model_name, 'CovEnable', 'off');
set_param(model_name, 'RecordCoverage', 'off');
set_param(model_name, 'SaveOutput', 'off');
set_param(model_name, 'SaveState', 'off');
set_param(model_name, 'SaveTime', 'off');
scope_blocks = find_system(model_name, 'FindAll', 'on', 'LookUnderMasks','all',  'FollowLinks', 'on', 'BlockType','Scope');
for i = 1:length(scope_blocks)
    set_param(scope_blocks(i),'Open','off');
    %set_param(scope_blocks(i),'Commented','on');
end
set_param(model_name, 'SignalLogging', 'on');

start_time = clock;
tic

[bt] = makeBlockTable(model_name, flag_to_print_structure);
[noo, nob, bt] = findMeasuringObjects(bt, flag_to_print_structure);
[bt, lt] = makeLoggerTable(bt, flag_to_print_structure);

set_param(model_name, 'Stoptime', num2str(stop_time));

% ここ調べること
%input_info = setInputInfo(model_name, bt, manual_limit);
fp = fopen('templates.txt', 'w');
%writeInputInfo(bt, input_info, fp);
% ここまで
input_info = 0;

number_of_dead_logic.DC = dead{1};
number_of_dead_logic.CC = dead{2};
number_of_dead_logic.MCDC = dead{3};

rng('shuffle', 'twister');

number_of_contributable_candidates = 0;
cell_of_params_of_testsuite = cell(1, max_number_of_candidates);
d_cv_all = -1;
c_cv_all = -1;
m_cv_all = -1;
previous_test_case_contributability = 0;

set_param(model_name, 'SimulationMode', simulation_mode);
if ~strcmp(simulation_mode, 'rapid')
    %set_param(model_name, 'FastRestart','on');
end

%for i = 1:max_number_of_candidates
    i = 1;
    
    % generate a candidate test randomly
    %cell_of_params_testcase_temp = set_rand(input_info, i, previous_test_case_contributability);
    cell_of_params_testcase_temp = 0;
    [simout, lt] = simWithLogging(model_name, lt);
    [cov, bt] = measureCoverage(bt, lt, simout);
    
    if i == 1
        cv_all = cov;
        
        [d_cv_all, c_cv_all, m_cv_all, cv_all] = getCoverageInfo(cv_all);
        
        number_of_contributable_candidates = number_of_contributable_candidates + 1;
        cell_of_params_of_testsuite(number_of_contributable_candidates) = {cell_of_params_testcase_temp};
        cv_all.TestSuite = cell_of_params_of_testsuite;
        previous_test_case_contributability = 1;
        
        if flag_to_print_coverage == 1
            fprintf('%d: dc = %d/%d, cc = %d/%d, mcdc = %d/%d\n', i, d_cv_all, noo.DC - number_of_dead_logic.DC, c_cv_all, noo.CC - number_of_dead_logic.CC, m_cv_all, noo.MCDC - number_of_dead_logic.MCDC);
        end
        
    else
        fprintf('no this path.');
    end
    
    % if the test suite fully covors DC/CC/MCDC cases.
    if d_cv_all + c_cv_all + m_cv_all + number_of_dead_logic.DC + number_of_dead_logic.CC + number_of_dead_logic.MCDC == noo.DC + noo.CC + noo.MCDC
        fprintf('Full-coverage test suite is generated!: %d test cases (out of %d candidates)\n', number_of_contributable_candidates, i);
        % break;
    end
    
    if etime(clock, start_time) > max_processing_time
        fprintf('Time out\n');
        %break;
    end
%end