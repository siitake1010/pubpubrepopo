import x10.util.Stack;
import x10.util.StringBuilder;

public class RPN {
  public static def getRpn(formula: String) {
    val seq: Rail[Char] = formula.chars();
    val res = new StringBuilder();
    val stack = new Stack[Char]();

    for (c in seq) {
      switch (c) {
        case '+':
        case '-':
          while (!stack.isEmpty()) {
            val s: Char = stack.peek();
            if (s == '*' || s == '/') {
              res.add(stack.pop());
            } else {
              break;
            }
          }
          stack.push(c);
          break;
        case '*':
        case '/':
        case '(':
          stack.push(c);
          break;
        case ')':
          while (!stack.isEmpty()) {
            val s: Char = stack.pop();
            if (s != '(') {
              res.add(s);
            } else {
              break;
            }
          }
          break;
        default:
          res.add(c);
          break;
      }
    }
    while (!stack.isEmpty()) {
      res.add(stack.pop());
    }
    return res.toString();
  }

  public static def calcRpn(rpn: String) {
    val str: Rail[Char] = rpn.chars();
    val res = new Stack[Long]();

    var a: Long;
    var b: Long;
    for (c in str) {
      switch (c) {
        case '+':
          a = res.pop();
          b = res.pop();
          res.push(b + a);
          break;
        case '-':
          a = res.pop();
          b = res.pop();
          res.push(b - a);
          break;
        case '/':
          a = res.pop();
          b = res.pop();
          res.push(b / a);
          break;
        case '*':
          a = res.pop();
          b = res.pop();
          res.push(b * a);
          break;
        default:
          if (!c.isWhitespace()) {
            res.push(Long.parse(c.toString()));
          }
          break;
      }
    }
    return res.pop();
  }

  static public def main(args: Rail[String]) {
    Console.OUT.println(getRpn("3+4*5"));
    Console.OUT.println(" = " + calcRpn(getRpn("3+4*5")).toString());
    Console.OUT.println(getRpn("3*4+5"));
    Console.OUT.println(" = " + calcRpn(getRpn("3*4+5")).toString());
    Console.OUT.println(getRpn("2*(5/4-3)+1"));
    Console.OUT.println(" = " + calcRpn(getRpn("2*(5/4-3)+1")).toString());
  }
}
