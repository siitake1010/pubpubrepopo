% 関数の引数予定
model_name = 'Amodel';
new_model_name = 'Bmodel';
% ここまで
% load systems
load_system(model_name);
new_system(new_model_name);
open_system(new_model_name);

% create subsystem
new_subsystem = [new_model_name, '/test_unit'];
add_block('built-in/Subsystem', new_subsystem);
Simulink.BlockDiagram.copyContentsToSubsystem(model_name, new_subsystem);

inport_num = length(find_system(new_subsystem, 'BlockType', 'Inport'));
outport_num = length(find_system(new_subsystem, 'BlockType', 'Outport'));

% test_unit -> outport
if (outport_num ~= 0)
    for i = 1:outport_num
        add_block('simulink/Sinks/Out1', [new_model_name, '/out', num2str(i)]);
        add_line(new_model_name, ['test_unit/', num2str(i)], ['out', num2str(i), '/1'], 'autorouting', 'on');
    end
end

% create size-type system
add_block('built-in/Subsystem', [new_model_name, '/Size-Type']);
cast_type = 'double';
for i = 1:inport_num
    add_block('simulink/Sources/In1', [new_model_name, '/Size-Type/in', num2str(i)]);
    add_block('simulink/Signal Attributes/Data Type Conversion', [new_model_name, '/Size-Type/Cast', num2str(i)]);
    add_block('simulink/Signal Attributes/Rate Transition', [new_model_name, '/Size-Type/Sync', num2str(i)]);
    add_block('simulink/Sinks/Out1', [new_model_name, '/Size-Type/out', num2str(i)]);
    set_param([new_model_name, '/Size-Type/Cast', num2str(i)], 'OutDataTypeStr', cast_type);
    add_line([new_model_name, '/Size-Type'], ['in', num2str(i), '/1'],  ['Cast', num2str(i), '/1'], 'autorouting', 'on');
    add_line([new_model_name, '/Size-Type'], ['Cast', num2str(i), '/1'], ['Sync', num2str(i), '/1'], 'autorouting', 'on');
    add_line([new_model_name, '/Size-Type'], ['Sync', num2str(i), '/1'], ['out', num2str(i), '/1'], 'autorouting', 'on');
end

% size-type -> test_unit
for i = 1:inport_num
    add_line(new_model_name, ['Size-Type/', num2str(i)], ['test_unit/', num2str(i)], 'autorouting', 'on');
end

% create signal builder
add_block('simulink/Sources/Signal Builder', [new_model_name, '/Inputs']);
for i = 2:inport_num
    signalbuilder([new_model_name, '/Inputs'], 'appendsignal', [0 10], [0 1]);
end

% signal builder -> size-type
for i = 1:inport_num
    add_line(new_model_name, ['Inputs/', num2str(i)], ['Size-Type/', num2str(i)], 'autorouting', 'on');
end

% TODO
% 関数化
% set_param (警告などの設定)